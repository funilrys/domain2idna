Code documentation
==================

.. automodule:: domain2idna
   :members: get

Helpers
-------

Problematic
^^^^^^^^^^^

How can we write, read and delete a file without having to write everytime the same thing?

Code documentation
^^^^^^^^^^^^^^^^^^

.. automodule::domain2idna.helpers
   :members: 


:code:`File()`
""""""""""""""

.. autoclass:: domain2idna.helpers.File
    :members:


Core
----

Code documentation
^^^^^^^^^^^^^^^^^^

.. automodule::domain2idna.core
   :members: 

.. autoclass:: domain2idna.core.Core
    :members:
