The tool to convert domains to the famous IDNA format.
======================================================

.. image:: https://travis-ci.com/funilrys/domain2idna.svg?branch=master
   :target: https://travis-ci.com/funilrys/domain2idna
.. image:: https://coveralls.io/repos/github/funilrys/domain2idna/badge.svg?branch=master
   :target: https://coveralls.io/github/funilrys/domain2idna?branch=master
.. image:: https://img.shields.io/github/license/funilrys/domain2idna.svg
   :target: https://github.com/funilrys/domain2idna/blob/master/LICENSE
.. image:: https://img.shields.io/github/release/funilrys/domain2idna.svg
   :target: https://github.com/funilrys/domain2idna/releases/latest
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/ambv/black


.. toctree::
   :maxdepth: 2
   :caption: Content

   installation
   usage
   code
   issues-reporting

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
